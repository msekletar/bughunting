#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "time"

class Scores
  def initialize(database)
    @db = database
    setup_schema
  end

  def set(player, task, score)
    @db.execute \
      "INSERT OR REPLACE INTO scores (player, task, score) VALUES (?, ?, ?)",
      [player, task, score]
  end

  def total(player)
    score = @db.get_first_value \
      "SELECT SUM(score) score FROM scores WHERE player like ?",
      [player]
    score || 0
  end

  def all_tasks(player)
    result = {}
    @db.execute "SELECT task, score FROM scores WHERE player like ?",  player do |task, score|
      result[task] = score
    end
    result
  end

  def task(player, task)
    score = @db.get_first_value \
      "SELECT score FROM scores WHERE player like ? AND task like ?",
      [player, task]
    score || 0
  end

  def ranking
    @db.execute(
      "SELECT p.player, p.name, p.email, sum(s.score) sum, strftime('%s', max(s.created_at)) last
      FROM players p LEFT JOIN scores s ON p.player like s.player GROUP BY p.player
      ORDER BY sum DESC, last ASC, p.created_at ASC"
    ).collect do |player, name, email, score, last_scoring|
      {
        :player => player,
        :name => name,
        :email => email,
        :score => score || 0,
        :last_scoring => last_scoring.nil? ? nil : Time.at(last_scoring.to_i),
      }
    end
  end

  private

  def setup_schema
    @db.execute "CREATE TABLE IF NOT EXISTS scores (
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
      player VARCHAR NOT NULL,
      task VARCHAR NOT NULL,
      score TINYINT NOT NULL,
      UNIQUE (player, task)
    )"
  end
end
