#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/check/task"
require "bughunting/web/config"
require "bughunting/web/hints"
require "bughunting/web/players"
require "bughunting/web/scores"

require "sqlite3"

class WebController
  attr_reader :config
  attr_reader :tasks, :scores, :hints, :players

  def initialize(config_filename)
    @config = WebConfig.load_file config_filename
    @tasks = Task.load_dir @config["paths.tasks"]

    @database = SQLite3::Database.new @config["results.database"]
    @scores = Scores.new @database
    @hints = Hints.new @database
    @players = Players.new @database
  end

  def tasks_list(player)
    tasks = []
    task_scores = @scores.all_tasks player
    @tasks.each do |name,task|
    tasks << {
      :name => name,
      :summary => task.config["summary"],
      :language => task.config["language"],
      :difficulty => task.config["difficulty"],
      :max_score => task.config["max_score"],
      :score => task_scores[name] || 0,
    }
    end
    tasks
  end

  def game_running?
    File.exists? @config["results.running"]
  end

  def start_game
    unless game_running?
      time = File.open( @config["results.running"], "w+" )
      time.write Time.now.to_i
      time.close
    end
  end

  def stop_game
    File.delete @config["results.running"] if game_running?
  end

  def archive_game
    #do we need archive folder?
    if game_running?

      system   "tar --remove-files -cvJf \
               #{@config["results.archive"]}/bh-game-`date +%F_%R`.tar.xz \
               #{@config["results.database"]} \
               #{@config["results.running"]} \
               #{@config["results.archive"]}/*"
    end
  end

  def machine_name(ip)
    @config["players"][ip] || ip
  end
end
