require 'bughunting/check/jail'
require 'bughunting/check/task'

RSpec.describe Jail do
  let(:task_path) { File.join(fixtures_path, 'cache', 'warm_up') }
  let(:task) { Task.new(task_path) }
  let(:jail) { Jail.new(nil) }
  let(:carrier) { double("RemoteCarrier") }

  describe '#create' do
    before do
      allow(carrier).to receive(:get) { true }
      allow(carrier).to receive(:patch)
    end

    after do
      FileUtils.rm_rf jail.path
    end

    context 'with task defaults' do
      before do
        jail.create(task, carrier)
      end

      it 'creates temporary directory' do
        expect(File).to exist(jail.path)
      end

      it 'requests player files' do
        expect(carrier).to have_received(:get).with(jail.exec_dir, task.config['check_files'])
      end

      it 'copies check files' do
        check_file = File.join(jail.exec_dir, task.config['check_script'])

        expect(File).to exist(check_file)
      end
    end

    context 'when copy_task_on_server is true' do
      before do
        task.config['copy_task_on_server'] = true
        jail.create(task, carrier)
      end

      it 'copies source files from task dir' do
        source_files_in_task_dir = Dir.children task.source_dir
        files_in_jail = Dir.children jail.exec_dir
        # We don't care about the check file
        files_in_jail.delete 'check.sh'
        # We expect to see the fix.patch in the jail dir
        source_files_in_task_dir << 'fix.patch'

        expect(files_in_jail).to match_array(source_files_in_task_dir)
      end
    end

    context 'task defines valid build commands' do
      before do
        task.config['build_commands'] << "touch foo_script.sh"
        jail.create(task, carrier)
      end

      it 'runs the commands' do
        script_path = File.join jail.exec_dir, 'foo_script.sh'
        expect(File).to exist(script_path)
      end
    end

    context 'task defines failing build command' do
      before do
        task.config['build_commands'] << "false foo"
      end

      it 'fails with a custom message' do
        expect { jail.create(task, carrier) }.to raise_error(RuntimeError, 'Cannot build task, call for help.')
      end
    end
  end

  describe '#save_patch_to_file' do
    let(:patch_path) { File.join jail.exec_dir, 'fix.patch' }
    let(:patch) { "--- a\n+++ b\ncc" }

    before do
      allow(carrier).to receive(:patch).and_return(patch)
    end

    after do
      FileUtils.remove_entry jail.path
    end

    context 'with diff of one file' do
      before do
        jail.send :save_patch, carrier
      end

      it 'saves patch into one file' do
        expect(Dir.children(jail.exec_dir)).to include 'fix.patch'
        expect(File.read(patch_path)).to eq patch
      end
    end

    context 'with diff of multiple files' do
      before do
        jail.send :save_patch, carrier
      end

      it 'saves patch into one file' do
        expect(Dir.children(jail.exec_dir)).to include 'fix.patch'
        expect(File.read(patch_path)).to eq patch
      end
    end
  end
end
